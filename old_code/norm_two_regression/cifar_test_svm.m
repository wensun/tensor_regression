rng(1010);
q = 2;

data = load('cifar_dataset.mat');

A_train_1 = double(data.X_train_1);
A_train_2 = double(data.X_train_2);
y_train_1 = int64(data.y_train_1);
y_train_2 = int64(data.y_train_2);

A = [A_train_1; A_train_2];
y = [y_train_1, y_train_2];
index_01 = (y == 1 | y == 0);
A_01 = A(index_01,:);
y_01 = y(1,index_01);

middle = int64(size(A_01,1)/2);
A_train_1 = A_01(1:middle,:);
A_train_2 = A_01(middle+1:size(A_01,1),:);
y_train_1 = y_01(1,1:middle);
y_train_2 = y_01(1,middle+1:size(y_01,2));

%SVD:
A_total = [A_train_1; A_train_2];
[U,D] = eig(A_total'*A_total);
trans = U(:,55:64)*inv(D(55:64,55:64)^0.5);
A_total_p = A_total*trans;

A_train_1 = A_total_p(1:size(A_train_1,1),:);
A_train_2 = A_total_p(size(A_train_1,1)+1:size(A_total_p,1),:);

tic
%svmstruct  = svmtrain([A_train_1; A_train_2], [y_train_1,y_train_2]);
x = [A_train_1;A_train_2]\(double([y_train_1,y_train_2]')-0.5);
%B = mnrfit([A_train_1;A_train_2], categorical([y_train_1,y_train_2]))
svm_train_time = toc


X_test_1 = double(data.X_test_1);
X_test_2 = double(data.X_test_2);
y_test_1 = int64(data.y_test_1);
y_test_2 = int64(data.y_test_2);
X_test = [X_test_1;X_test_2];
X_test = X_test*trans;
y_test = [y_test_1, y_test_2];
index_01 = (y_test == 0 | y_test == 1);

X_test = X_test(index_01,:);
y_test = y_test(1,index_01);
middle = int64(size(X_test,1)/2);
X_test_1 = X_test(1:middle,:);
X_test_2 = X_test(middle+1:size(X_test,1),:);
y_test_1 = y_test(1,1:middle);
y_test_2 = y_test(1,middle+1:size(y_test,2));

%X_test_1 = A_train_1;
%X_test_2 = A_train_2;
%y_test_1 = y_train_1;
%y_test_2 = y_train_2;


%pred_test_1 = svmclassify(svmstruct, X_test_1);
%pred_test_2 = svmclassify(svmstruct, X_test_2);
pred_test_1 = (X_test_1*x>0.0);
pred_test_2 = (X_test_2*x>0.0);
%[~,pred_test_1] = max(mnrval(B, X_test_1), [], 2);
%[~,pred_test_2] = max(mnrval(B, X_test_2), [], 2);


b_test = zeros(size(y_test_2,2), size(y_test_1,2));
pred_test=zeros(size(pred_test_2,1),size(pred_test_1,1));
for i = 1 : size(y_test_1,2)
    b_test(:,i) = (y_test_2 == y_test_1(i));
end
for i = 1 : size(pred_test_1,1)
    pred_test(:,i) = (pred_test_2 == pred_test_1(i));
end
b_test = b_test(:);
pred_test = pred_test(:);

svm_err = sum(b_test ~= pred_test)/size(b_test,1)






