function A = row_wise_kron(A1, A2 )

    n = size(A1,1);
    r = size(A1,2);
    
    tmp = reshape(A1, n, r, 1).*permute(reshape(A2, n,r,1), [1,3,2]);
    tmp = permute(tmp, [1,3,2]);
    
    A = reshape(tmp, n, r^2);
end

