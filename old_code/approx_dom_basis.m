function [Z, R, V] = approx_dom_basis(A, k, sfun, params)
% Z = approx_dom_basis(A, k, sfun, params)
%
% Form a matrix Z with k columns that approximates the k-dominant subspace spanned
% by A (or, a function of A). Z has orthonormal columsn.
% 
% - sfun is a function used to sketchs rows of a matrix.
% - params is passed to sfun. That is sfun is called sfun(A, s, 1, params) 
%   and sfun(A, t, 2, params).
%
%
% If s = Omega(k/eps) and t = Omega(k/eps^2) we are guarenteed to find
% a Z such that
%      ||Z * Z' * A - A||_F <= (1+eps)||A_k - A||_F
%
% R and V:
% Represent  the transformation used to find Z. Specifically,
%     Z = sfun(A, 1, params) * inv(R) * V
%
% Imporant note: sfun can implicitly sketches a function of A!


X = sfun(A, 1, params);             
Y = sfun(A, 2, params);             
[U, R] = qr(X, 0);
[M, S, N] = svd(U' * Y);
V = M(:, 1:k);
Z = U * V;
