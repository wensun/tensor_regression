function index = sample_from_basis(A, R,r)
    %A: nxr
    %the well-behaved basis would be A*R^{-1}. (nxrxd)
    %but we are not going to compute A*R^{-1} directly. 
    %we create another sketch S on the right hand side of AR^{-1} S
    % complexity: d*d*log(n) + n*r*log(n)
    %r: the expected number of rows we want from A:
    
    d = size(R,2);
    n = size(A,1);
    t = 1.*log(n);
    
    S = randn(d,int32(t))*(t^0.5);
    SA = A*(R\S);  %A*R^-1 S
    
    lone_norms = sum(abs(SA),2);
    ps = min(r*lone_norms/sum(lone_norms), ones(n,1));
    sampled_p = rand(n,1);
    index = (sampled_p < ps); 
end

