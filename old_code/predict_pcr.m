function Yp = predict_pcr(Xt, model)
% Yp = predict_pcr(Xt, model)
%
% Predict on Xt using the approximate PCR model in model.

Yp = model.project(Xt) * model.w;
