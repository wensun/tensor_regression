function x = norm_one_reg(A_cell, b, expected_nr, m, k)
    %A_cell, a list of matrices
    %m: the target row number for tensorsketch for each row-block
    %k: divide each matrix A_i into k sub row-block, w_i = n_i/k.
    %r: expected rows
    %b: d-dim, Ax- b
    q = length(A_cell);
    ws = zeros(q,1);
    ns = zeros(q,1);
    rs = zeros(q,1);
    for i = 1 : q
        ns(i) = size(A_cell{i},1);
        rs(i) = size(A_cell{i},2);
        ws(i) = ns(i)/k;
    end
    r = prod(rs);
    %n = prod(ns);
    
    SA = block_wise_tensor_sketch(A_cell, m, k, ws,r); %(k^q)m x r.
    disp('done block-wise tensor sketch')
    
    R = well_behaved_basis(SA); %change basis. R:rxr.
    disp('done well behaved basis construction')
    
    A = A_cell{1};
    for i = 2 : q
        A = kron(A, A_cell{i});
    end
    r_index = sample_from_basis(A,R,expected_nr);
    disp('done sampling from basis')
    sub_A = A(r_index, :);
    sub_b = b(r_index,1);
 
    fprintf('start cvx with %d rows.\n', size(sub_A,1))   
    x = solve_ADR(sub_A, sub_b);
    disp('done linear programming')
end


function x = solve_ADR(A, b)
    %||Ax-b||_1 using linear programming:
    [n d] = size(A);
    cvx_begin quiet
        variable x(d)
        minimize ( norm ( A*x - b , 1) )
    cvx_end
end


function sketched_result = block_wise_tensor_sketch(A_cell, m, k, ws,r)
    %A_cell: q matrices, m: target row number for each block
    %k: divde each matrix A_i to k sub-matrix, w_i= n_i/k (we assume n_i%k=0)
    %r: r1r2..rq
    q = length(A_cell);
    nr = (k^q)*m;
    sketched_result = zeros(nr, r);
    block_combs = zeros(k^q, q);
    ds = ones(q,1)*k;
    sub_A_cell = cell(q);
    for i = 1 : k^q
        block_combs(i,:) = convert_2_row(i,ds);
        for j = 1 : q
            A = A_cell{j};
            index = block_combs(i,j);
            sub_A_cell{j} = A((index-1)*ws(j)+1:index*ws(j),:);
        end
        [S,~] = sketch_tensor_kron(sub_A_cell, m);
        sketched_result((i-1)*m+1:i*m,:) = S;
    end
end
    

