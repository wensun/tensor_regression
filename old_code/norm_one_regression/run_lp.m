
rng(1000);
q = 2; 
n_i = 500;
r_i = 20;
n = n_i^q;
r = r_i^q;
A_cell = cell(q);
k = 5;

trials = 5;
time_trials = zeros(1,trials);
err_trials = zeros(1,trials);

for t = 1 : trials
    for i = 1 : q
        A_cell{i} = randn(n_i, r_i);
    end
    b = randn(n,1);
    
    tic
    disp('forming tensor...')
    A = A_cell{1};
    for i = 2 : q
        A = kron(A, A_cell{i});
    end

    disp('start cvx...')
    cvx_begin quiet
        variable x(size(A,2))
        minimize ( norm ( A*x - b , 1) )
    cvx_end
    disp('done...')

    time_trials(t) = toc;
    err_trials(t) = sum(abs(A*x-b))
end

avg_time = mean(time_trials)
avg_err = mean(err_trials)
