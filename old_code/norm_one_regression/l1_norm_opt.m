function [x] = l1_norm_opt(A, b)
	
	[n,d] = size(A);
	cvx_begin quiet
		variable x(d)
		minimize ( norm (A*x - b, 1) )
	cvx_end

end
