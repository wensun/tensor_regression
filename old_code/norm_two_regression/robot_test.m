data = load('robot_dataset.mat');

X = data.X;
b = data.b';

A_cell = {X,X};

epsilon = 0.1;
m = (49./epsilon)^2;

tic
x = norm_two_reg(A_cell, b, m);
sketch_t = toc


disp('start brute force..')
tic
x_opt = kron(A_cell{1},A_cell{2})\b;
bf_t = toc;


%train error:
sketch_err = norm(kron(A_cell{1},A_cell{2})*x - b)
opt_err = norm(kron(A_cell{1},A_cell{2})*x_opt - b) 
