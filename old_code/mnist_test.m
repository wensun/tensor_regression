function mnist_test_poly(rk, t, s, q, c, seed)

if (nargin < 4)
   q = 3;
end

if (nargin < 5)
   c = 1;
end

if (nargin < 6)
   seed = 32326;
end


randn('seed', seed);
rand('seed', seed);

load datasets/mnist.mat;

[n, d] = size(X);
r = size(Xt, 1);

% Convert Y to a set of +1/-1 vectors for each label
Ymin = min(Y);
k = max(Y) - min(Y) + 1;
rY = -ones(n, k);
for j = 1:n
    rY(j, Y(j)+1) = 1;
end

tic;

[model, Yp_train0] = approx_poly_kpcr(X, rY, rk, q, c, t, s);
[dummy, Yp_train] = max(Yp_train0, [], 2);
Yp_train = Yp_train -1 + Ymin;

Yp0 = predict_pcr(Xt, model);
[dummy, Yp] = max(Yp0, [], 2);
Yp = Yp -1 + Ymin;

toc

% Compute accuracy
train_accuracy = sum(Yp_train == Y) / length(Y)
test_accuracy = sum(Yp == Yt) / length(Yt)
