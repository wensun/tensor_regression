function X = apply_ppt(A, T)
% X = apply_ppt(A, T)
% 
% Apply the PPT transform T to A.

m = size(A, 1);
n = size(A, 2);

P = ones(T.t, n); %txn.

if (T.c > 0)
   A = [A; T.c * ones(1, n)];
   m = m + 1;
end

%s = sqrt(T.t);
for i=1:T.q
    %CountSketch C: t x m, and A is mxn.
    CA = vcountsketch(A, T.t, T.map(:, i), T.sg(:, i));   
    P = fft(CA) .* P; %CA: txn
end

X = ifft(P);




  