function U = well_behaved_basis(A)
%generate l1 sparse embeding matrix using Cauchy distribution 
%(student t distribution wit degree =1 )

d = size(A,2);
n = size(A,1);

r = d*log(d); %dimension for l1-sparse embeding.
S = trnd(1, r, n);
[Q,R] = qr(S*A,0);
U = (R'\A')';
end

function index = sample_from_basis(A, R)
    %the well-behaved basis would be A*R^{-1}.
    %but we are not going to compute A*R^{-1} directly. 
    %we create another sketch on the right hand side of AR^{-1}
    
    


end



