function R = well_behaved_basis(A)
%generate l1 sparse embeding matrix using Cauchy distribution 
%(student t distribution wit degree =1 )

d = size(A,2);
n = size(A,1);

r = int32(d*log(d)); %dimension for l1-sparse embeding.
S = trnd(1, r, n);
[Q,R] = qr(S*A,0);
%U = (R'\A')';
end



%A(index,:)



