
rng(1000);
k = 2;


m = 2000 %16000 %12000 %8000

trials = 1;
time_trials = zeros(1,trials);

rt_old_trials = zeros(1,trials);
rt_new_trials = zeros(1,trials);

re_old_trials = zeros(1,trials);
re_new_trials = zeros(1,trials);

rt_wrt_old_trials = zeros(1,trials);
re_wrt_old = zeros(1,trials);

for t = 1 : trials
    
    t


    A_train_1 = randn(300, 15);
    A_train_2 = randn(300, 15);
    A_cell = {A_train_1, A_train_2};
    b_train = randn(300^2, 1);

    n = size(A_train_1, 1);
    r = size(A_train_1, 2);



    disp("running old algorithm...")
    tic
    c = 1;
    epsilon = 0.5;
    expected_nr = m; %final number of sampled rows for LP
    m_tmp = int32(c*r^2/(epsilon^2));
    x_old= norm_one_reg(A_cell, b_train, expected_nr, m_tmp, k);
    time_sketch_old = toc

    disp("running new algorithm...")
    tic
    x_new = norm_one_reg_new(A_cell, b_train, m);
    time_sketch_new = toc

    disp("compute the optimal solution...")
    tic
    kron_A1A2 = kron(A_train_1, A_train_2);
    x_opt = l1_norm_opt(kron_A1A2, b_train);
    time_adr = toc

    disp("computing statistics...")
    train_loss_old = sum(abs(kron_A1A2*x_old - b_train));
    train_loss_new = sum(abs(kron_A1A2*x_new - b_train));
    train_loss_opt = sum(abs(kron_A1A2*x_opt - b_train));

    r_e_old = 100*abs(train_loss_old - train_loss_opt)/train_loss_opt;
    r_e_new = 100*abs(train_loss_new - train_loss_opt)/train_loss_opt;

    r_t_old = time_sketch_old / time_adr;
    r_t_new = time_sketch_new / time_adr;
    r_t_wrt_old = time_sketch_new / time_sketch_old;

    re_old_trials(t) = r_e_old;
    re_new_trials(t) = r_e_new;
    rt_old_trials(t) = r_t_old;
    rt_new_trials(t) = r_t_new;
    rt_wrt_old_trials(t) = r_t_wrt_old;

end

%re_old_mean = mean(re_old_trials);
re = mean(re_new_trials);
reprime = mean(re_old_trials);
%rt_old_mean = mean(rt_old_trials);
rt = mean(rt_new_trials);
rtprime = mean(rt_wrt_old_trials);



