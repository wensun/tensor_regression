function [ x ] = norm_one_reg_new( A_cell, b, m )
%A_cell: contaisn two matrices each has dimension n x r
%b: n^2, Ax -b
%m: sketch target

    n = size(A_cell{1},1);
    r = size(A_cell{1},2);
    
    
    R1 = well_behaved_basis(A_cell{1});
    R2 = well_behaved_basis(A_cell{2});
    
    A1R1_inv = ((R1')\(A_cell{1}'))'; % ((R1^T)^{-1} A1^T)^T = A1 R1^-1
    A2R2_inv = ((R2')\(A_cell{2}'))'; 
    size(A1R1_inv)
    d = size(A1R1_inv,2);
    
    %l1 leverage score
    disp("computing l1 leverage scores")
    lone_norms_1 = sum(abs(A1R1_inv), 2);
    normalized_lone_1 = lone_norms_1/sum(lone_norms_1);
    lone_norms_2 = sum(abs(A2R2_inv), 2);
    normalized_lone_2 = lone_norms_2/sum(lone_norms_2);
    
    size(lone_norms_1)
    size(lone_norms_2)
    
    
    %sample m indexes from normalized_lone_1
    disp("constructing sub matrices")
    index_1 = randsample(size(lone_norms_1,1), m, true, normalized_lone_1);
    index_2 = randsample(size(lone_norms_2,1), m, true, normalized_lone_2);
    %combined_index = index_1 + n * index_2; 
    combined_index = (index_1-1)*n + index_2;
    
    %sub_A = row_wise_kron(A1R1_inv(index_1,:), A2R2_inv(index_2,:)); %perm row-wise kron product. 
    sub_A = row_wise_kron(A_cell{1}(index_1,:), A_cell{2}(index_2,:));
    size(sub_A)
    
    sub_b = b(combined_index, :);
    size(sub_b)
    
    
    fprintf('start cvx with %d rows.\n', size(sub_A,1))   
    x = solve_ADR(sub_A, sub_b);
    disp('done linear programming')
    
end

function x = solve_ADR(A, b)
    %||Ax-b||_1 using linear programming:
    [n d] = size(A);
    cvx_begin quiet
        variable x(d)
        minimize ( norm ( A*x - b , 1) )
    cvx_end
end


