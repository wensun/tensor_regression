function mnist_test_poly(t, q, c, seed)

if (nargin < 2)
   q = 3;
end

if (nargin < 3)
   c = 1;
end

if (nargin < 4)
   seed = 32326;
end


randn('seed', seed);
rand('seed', seed);

load datasets/mnist.mat;

%disp('Linear SVM')
%tic;
%model = train(Y, sparse(X), '-s 2 -q'); 
%[Yp, accuracy] = predict(Yt, sparse(Xt), model);
%toc;

disp('Sketched Polyomial SVM')
tic;
T = create_ppt(size(X, 2), t, q, c);
SX = apply_ppt(X', T)';
model = train(Y, sparse(SX), '-s 2 -q'); 
SXt = apply_ppt(Xt', T)';
[Yp, accuracy] = predict(Yt, sparse(SXt), model);
toc;
