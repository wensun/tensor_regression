function x = norm_two_reg(A_cell, b, m)
    %A_cell, a list of matrices
    %m: the target row number for tensorsketch
    %b: d-dim, Ax- b
    
    q = length(A_cell);
    ns = zeros(q,1);
    rs = zeros(q,1);
    for i = 1 : q
        ns(i) = size(A_cell{i},1);
        rs(i) = size(A_cell{i},2);
    end
    %r = prod(rs);
    %n = prod(ns);
    
    [SA,Cs] = sketch_tensor_kron(A_cell, m);
    
    %size(SA)
    
    joint_Cs = joint_hash(Cs);
    SA = vcountsketch(kron(A_cell{1},A_cell{2}), joint_Cs.t, joint_Cs.map, joint_Cs.sg);
    disp('done sketch on A')
    Sb = vcountsketch(b, joint_Cs.t, joint_Cs.map, joint_Cs.sg);    
    disp('done tensor sketch')
    size(Sb)
    
    %dd = SA - SA2;
    
    x = SA\Sb;
    
    %x = solve_LSR(SA, Sb);
    disp('done LSR')
end


function x = solve_LSR(A, b)
    %||Ax-b||_2:
    %lambda = 1e-8;
    %Ap = [A; lambda^0.5*eye(size(A,2))];
    %bp = [b; zeros(size(A,2),1)];
    %x = pinv(Ap)*bp;
    x = A\b;
end

