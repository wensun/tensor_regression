function T_joint = joint_hash(Cs)

q = length(Cs);
m = Cs{1}.m;
t = Cs{1}.t;
joint_m = Cs{1}.m^q; %q =2 
T_joint.map = zeros(joint_m,1);
T_joint.sg = zeros(joint_m,1);

pos = 1;
for i = 1 : m
    for j = 1 : m
        T_joint.map(pos) = mod(Cs{1}.map(i,1) + Cs{2}.map(j,1), t)+1;
        T_joint.sg(pos) = sign(Cs{1}.sg(i,1)*Cs{2}.sg(j,1));
        pos = pos + 1;
    end
end

T_joint.m = joint_m;    
T_joint.q = 1;
T_joint.c = 0;
T_joint.t = t;


