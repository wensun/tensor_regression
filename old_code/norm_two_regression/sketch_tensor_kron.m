function [S, Cs] = sketch_tensor_kron(A_cell, m)

    q = length(A_cell);
    ds = zeros(q,1);
    ns = zeros(q,1);
    for i = 1 : q
        ds(i) = size(A_cell{i},2);  %d_i
        ns(i) = size(A_cell{i},1);  %n_i
    end

    %create q hash (CountSketch):
    for i = 1 : q
        T.map = randi(m,ns(i),1);
        T.sg = sign(randn(ns(i),1));
        T.t = m;
        T.m = ns(i);
        Cs{i} = T;
    end
    
    d = prod(ds);
    S = zeros(m, d);
    rows_comb = zeros(d,q);
    for i = 1 : d
        rows_comb(i,:) = convert_2_row(i, ds);
    end
    
    for i = 1 : d
        p = ones(m,1);
        for j = 1 : q
            A = A_cell{j};
            c = A(:,rows_comb(i,j));
            sket = Cs{j};
            cc = vcountsketch(c, sket.t, sket.map, sket.sg);
            p = fft(cc).*p;
        end
        S(:,i) = ifft(p);
    end 
end