function row_indx = convert_2_row(index, caps)
    k = length(caps);
    row_indx = zeros(k,1);
    rm = index-1; %starts from zero.
    for i = k : -1 : 1
       row_indx(i) = mod(rm, caps(i));
       if rm - row_indx(i) == 0
           break
       else
           rm = (rm - row_indx(i))/caps(i);
       end
    end
    row_indx = row_indx + 1; %convert to row index. 
end
