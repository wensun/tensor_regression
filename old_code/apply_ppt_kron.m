function [S, Cs] = apply_ppt_kron(A_cell, m)
%currently it only support 2 matrix kronecker product:

q = length(A_cell);
ds = zeros(q,1);
inds = ones(q,1);
ns = zeros(q,1);
for i = 1 : q
  ds(i) = size(A_cell{i},2);  %d_i
  ns(i) = size(A_cell{i},1);  %n_i
end

%create q hash (CountSketch):
for i = 1 : q
    T.map = randi(m,ns(i),1);
    T.sg = sign(randn(ns(i),1));
    T.t = m;
    T.m = ns(i);
    Cs{i} = T;
end

n = prod(ns);
d = prod(ds);
S = zeros(m, d);

pos = 1;
while true
    p = ones(m,1);
    for i = 1 : q
        A = A_cell{i};
        c = A(:,inds(i));
        sket = Cs{i};
        cc = vcountsketch(c, sket.t, sket.map, sket.sg);
        p = fft(cc).*p;
    end
    S(:,pos) = ifft(p);
    pos = pos + 1;
    if prod(inds) == d
        break
    else
        inds = update_index(inds, ds);
    end
        
end

function new_inds = update_index(inds, ds)
new_inds = inds;
q = size(ds,1);
pos = -1;
for i = q:-1:1
    if inds(i) < ds(i)
        pos = i;
        break
    end
end
new_inds(pos) = new_inds(pos)+1;
if pos + 1 <= q
    new_inds(pos+1:q) = 1;
end

    




 