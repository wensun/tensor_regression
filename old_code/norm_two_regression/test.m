rng(1010)

%A_train_1 = randn(300, 15);
%A_train_2 = randn(300, 15);

%A_cell = {A_train_1, A_train_2};

%d = size(A_train_1,2)*size(A_train_2,2);
%b_train = randn(300^2, 1);

s = 64%127; %110; %90
m = s^2;

trials = 5;

rt_old_trials = zeros(1,trials);
rt_new_trials = zeros(1,trials);

re_old_trials = zeros(1,trials);
re_new_trials = zeros(1,trials);

rt_wrt_old_trials = zeros(1,trials);
re_wrt_old = zeros(1,trials);

for t = 1: trials
	
	t

	A_train_1 = randn(300, 15);
	A_train_2 = randn(300, 15);
	A_cell = {A_train_1, A_train_2};
	d = size(A_train_1,2)*size(A_train_2,2);
	b_train = randn(300^2, 1);

	n = size(A_train_1,1);
	r = size(A_train_1,2);

	disp("running the old algorithm...")
	tic
	x_old = norm_two_reg(A_cell, b_train, m);
	time_sketch_old = toc

	disp("running the new algorithm...")
	tic

	%default, we first do countsketch with 1/3 of total dimension, then apply
	%Gaussian sketch
	x_new = norm_two_reg_new(A_cell, b_train, ceil(n*3./4.), s); 
	time_sketch_new = toc

	disp('compute the optimal solution')
	tic
	kron_A1A2 = kron(A_cell{1}, A_cell{2});
	x_opt  = kron_A1A2\b_train;
	time_lsr = toc


	disp('computing statistics...')
	train_loss_old = norm(kron_A1A2*x_old - b_train);
	train_loss_new = norm(kron_A1A2*x_new - b_train);
	train_loss_opt = norm(kron_A1A2*x_opt - b_train);

	r_e_old = 100*abs(train_loss_old - train_loss_opt)/train_loss_opt;
	r_e_new = 100*abs(train_loss_new - train_loss_opt)/train_loss_opt;


	r_t_old = time_sketch_old / time_lsr;
	r_t_new = time_sketch_new / time_lsr;
	r_t_wrt_old = time_sketch_new / time_sketch_old;

	re_old_trials(t) = r_e_old;
	re_new_trials(t) = r_e_new;
	rt_old_trials(t) = r_t_old;
	rt_new_trials(t) = r_t_new;
	rt_wrt_old_trials(t) = r_t_wrt_old;

end

reprime = mean(re_old_trials)
re = mean(re_new_trials)
%rt_old_mean = mean(rt_old_trials)
rt = mean(rt_new_trials)
rtprime = mean(rt_wrt_old_trials)



