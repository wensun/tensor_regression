#include "mex.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/timeb.h>

/*this computes countsketch: SA, where A is mxn, and S is sxm 
 and S is defined using hash functions: map and sg in T
 */
void mexFunction(int nargout, mxArray *argout[],
    int nargin, const mxArray *argin[])
{
    long m, n, i, j, r, s;
    long mapped;
    double *map, *sign;
    double *A, *S, *pr;
    mwIndex *jc, *ir;
    double v, vv;
    int q;

    m = mxGetM(argin[0]);
    n = mxGetN(argin[0]);


    s = (long)mxGetScalar(argin[1]);
    map = mxGetPr(argin[2]);
    sign = mxGetPr(argin[3]);

    if (nargin >= 5)
        q = (int)mxGetScalar(argin[4]);
    else
        q = 1;

    argout[0] = mxCreateDoubleMatrix(s, q * n, mxREAL);
    S = mxGetPr(argout[0]);
    memset(S, 0, sizeof(double) * s * q * n);

    if (!mxIsSparse(argin[0])) {
        /* Dense */
        A = mxGetPr(argin[0]);
        for(j = 0; j < n; j++)
            for(i = 0; i < m; i++) {
                mapped = (long)map[i] - 1;
                vv = A[j * m + i];
                v = vv;
                for(r = 0; r < q; r++) {
                    S[(j * q + r) * s + mapped] += sign[i] * v;
                    v *= vv;
                }
            }
    } else {
        /* Sparse */
        pr = mxGetPr(argin[0]);
        ir = mxGetIr(argin[0]);
        jc = mxGetJc(argin[0]);
        for(j = 0; j < n; j++)
            for(i = jc[j]; i < jc[j+1]; i++) {
                mapped = (long)map[ir[i]] - 1;
                vv = pr[i];
                v = vv;
                for(r = 0; r < q; r++) {
                    S[(j * q + r) * s + mapped] += sign[ir[i]] * v;
                    v *= vv;
                }
            }
    }
}
