rng(1010);
q = 2;

data = load('cifar_dataset.mat');

A_train_1 = double(data.X_train_1);
A_train_2 = double(data.X_train_2);
y_train_1 = int64(data.y_train_1);
y_train_2 = int64(data.y_train_2);

A = [A_train_1; A_train_2];
y = [y_train_1, y_train_2];
index_01 = (y == 1 | y == 0);
A_01 = A(index_01,:);
y_01 = y(1,index_01);

middle = int64(size(A_01,1)/2);
A_train_1 = A_01(1:middle,:);
A_train_2 = A_01(middle+1:size(A_01,1),:);
y_train_1 = y_01(1,1:middle);
y_train_2 = y_01(1,middle+1:size(y_01,2));

%SVD:
A_total = [A_train_1; A_train_2];
[U,D] = eig(A_total'*A_total);
trans = U(:,55:64)*inv(D(55:64,55:64)^0.5);
A_total_p = A_total*trans;

A_train_1 = A_total_p(1:size(A_train_1,1),:);
A_train_2 = A_total_p(size(A_train_1,1)+1:size(A_total_p,1),:);

%A_train_1 = randn(700,10);
%A_train_2 = randn(700,10);

A_cell = {A_train_1,A_train_2};

d = size(A_train_1,2)*size(A_train_2,2)
%process b
b_train = zeros(size(y_train_2,2), size(y_train_1,2));
for i = 1 : size(y_train_1,2)
    b_train(:,i) = double(y_train_2 == y_train_1(i));
end
b_train = b_train(:)-0.5;
%b_train = kron(A_cell{1},A_cell{2})*rand(100,1) + randn(700*700,1);
%b_train = randn(700^2,1);

epsilon = 0.5;
r = 10;
m = 12*(r^2/epsilon)^2;

tic
x = norm_two_reg(A_cell, b_train, m);
time_sketch = toc


tic
disp('compute the optimal solution')
x_opt  = kron(A_cell{1},A_cell{2})\b_train;
disp('done computing the optimal solution')
time_lsr = toc


%disp('train svm')
%svmstruct = svmtrain([A_train_1;A_train_2], [y_train_1,y_train_2]);
%disp('done svm')


train_loss = norm((kron(A_cell{1},A_cell{2})*x) - b_train)
train_opt_loss = norm(kron(A_cell{1},A_cell{2})*x_opt - b_train)

X_test_1 = double(data.X_test_1);
X_test_2 = double(data.X_test_2);
y_test_1 = int64(data.y_test_1);
y_test_2 = int64(data.y_test_2);
X_test = [X_test_1;X_test_2];
X_test = X_test*trans;
y_test = [y_test_1, y_test_2];
index_01 = (y_test == 0 | y_test == 1);

X_test = X_test(index_01,:);
y_test = y_test(1,index_01);
middle = int64(size(X_test,1)/2);
X_test_1 = X_test(1:middle,:);
X_test_2 = X_test(middle+1:size(X_test,1),:);
y_test_1 = y_test(1,1:middle);
y_test_2 = y_test(1,middle+1:size(y_test,2));

%X_test_1 = A_train_1;
%X_test_2 = A_train_2;
%y_test_1 = y_train_1;
%y_test_2 = y_train_2;


X = kron(X_test_1,X_test_2);
%y_test_1 = int64(data.y_test_1(1:200));
%y_test_2 = int64(data.y_test_2(1:200));
b_test = zeros(size(y_test_2,2), size(y_test_1,2));
for i = 1 : size(y_test_1,2)
    b_test(:,i) = (y_test_2 == y_test_1(i));
end
b_test = b_test(:);

%predict:
pred_b = ((X*x)>=0.0);
err = sum(pred_b ~= b_test)/size(pred_b,1)

pred_b_opt = ((X*x_opt)>=0.0);
err_opt = sum(pred_b_opt ~= b_test)/size(pred_b_opt,1)

%test svm:
%svm_pred = svmclassify(svmstruct, [X_test_1;X_test_2]);
%err_svm = sum(svm_pred ~= b_test)/size(b_test,1)







