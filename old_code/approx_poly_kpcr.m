function [model, Yp] = approx_poly_kpcr(X, Y, rk, q, c, t, s)
% model = approx_poly_kpcr(X, Y, rk, q, c, t)
%
% Compute an approximate KPCR w/ polynomial kernel for regressing X
% on Y. 
%
% Input:
%   X, Y - matrix and righthand side.
%   rk - rank of the PCR.
%   q, c - degree and constant of the polynomial kernel
%   t,s - Sketch parameters. Should be smaller than d^q where
%         d is the number of columns in X. Theoertically should be rather
%         large (d / eps^2), but in practice a small factor of d might work.
% Output: 
%   model the can be used to predict.
%   Predicition on the input set.

d = size(X, 2);

% Find basis
p{1} = create_ppt(d, t, q, c); 
p{2} = create_ppt(d, s, q, c);
sketch = @(A, e, p) apply_ppt(A', p{e})';
[Z, R, V] = approx_dom_basis(X, rk, sketch, p);

% Solve the regression
w0 = Z' * Y;
w = R \ (V * w0);

% Keep the model
model.R = R;
model.V = V;
model.w0 = w0;
model.w = w;
model.project = @(Xt) apply_ppt(Xt', p{1})';

Yp = Z * w0;


