function [model, Yp] = poly_kpcr(X, Y, rk, q, c, t, s)
% model = poly_kpcr(X, Y, rk, q, c, t)
%
% Compute an KPCR w/ polynomial kernel for regressing X
% on Y. 
%
% Input:
%   X, Y - matrix and righthand side.
%   rk - rank of the PCR.
%   q, c - degree and constant of the polynomial kernel
%
% Output: 
%   model the can be used to predict.
%   Predicition on the input set.

n = size(X, 1);
d = size(X, 2);

%K = (X * X' + c * ones(n)) .^ q;
%save K.mat K

load K.mat

[U, E] = eig(K);

save UU.mat U E

[ee, ord] = sort(diag(E), 1, 'descend') ;

D = diag(sqrt(ee(1:rk)));
V = U(:, ord(1:rk));
Z = V * D;  

% Solve the regression
w0 = Z \ Y;
w = V * (D \ w0);

% Keep the model
model.D = D;
model.V = V;
model.w0 = w0;
model.w = w;
model.project = @(Xt)  (Xt * X' + c * ones(size(Xt, 1), n)) .^ q;

Yp = Z * w0;
