function [ x ] = norm_two_reg_new( A_cell, b, m1, m2 )
% A_cell, it contains 2 matrix, each with nxd
%m1: the target row number of CountSketch for each matrix in A_Cell
%m1: the target row number of Gaussian Sketch, default m2 < m1, for each matrix in A_cell
%d: Ax - b

    q = length(A_cell);
    n = size(A_cell{1}, 1);
    r = size(A_cell{1}, 2); 
    
    disp("perform CountSketches")
    CS_1 = generate_CountSketch_map(n, r, m1);
    CS_2 = generate_CountSketch_map(n, r, m1);
    SA_1 = vcountsketch(A_cell{1}, CS_1.t, CS_1.map, CS_1.sg); %m1 x r
    SA_2 = vcountsketch(A_cell{2}, CS_2.t, CS_2.map, CS_2.sg); %m1 x r
    
    disp("Perform Gaussian Sketches")
    G_1 = Gaussian_sketch(m1, m2);  %default m2 < m1
    G_2 = Gaussian_sketch(m1, m2);
    GSA_1 = G_1*SA_1; %m2 x r
    GSA_2 = G_2*SA_2; %m2 x r
   
    size(GSA_1)
    size(GSA_2)
    
    %reshape b and do sketch on both sides:
    disp("Perform Gaussian+CountSketch on b")
    B = reshape(b, n, n);
    left = G_1*vcountsketch(B, CS_1.t, CS_1.map, CS_1.sg);
    right = G_2*vcountsketch(left', CS_2.t, CS_2.map, CS_2.sg); 
    GS_B = right';
    %GS_b = vec(GS_B); %m2^2 x 1 vector.
    GS_b = reshape(GS_B, numel(GS_B), 1);
    
    size(GS_b)
    
    disp("solve LSR")
    x = (kron(GSA_1, GSA_2))\GS_b; %m2^2 for kron(GSA_1, GSA_2)
    disp("done LSR")

end


function [Cs] = generate_CountSketch_map(n, r, m)
    %n,r: size of the original matrix, n x r
    %m: cs target dimension
    
    T.map = randi(m, n, 1); %positions in each column 
    T.sg = sign(randn(n, 1)); %weother or not -1, or +1
    T.t = m; %target dimension
    T.m = n; %original dim. 
    Cs = T;  %map return
    
end

function [G_mat] = Gaussian_sketch(n, m)
    %A: original matrix 
    %m: Gaussian target dimension
    
    %generate a mxn Gaussian matrix:
    sigma = 1/n^0.5;
    G_mat = sigma*randn(m, n);

end
