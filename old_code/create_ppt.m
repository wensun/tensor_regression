function T = create_ppt(m, t, q, c)
% T = create_ppt(m, t, q, c)
%
% Create a Pahm-Pagh Transform for degree q and constant c, from m dimensions 
% to t dimensions
%in this code, we implicitly assumed that the tensor A1XA2X...,Aq, they
%have the same dimension: 

if (nargin > 3 && c > 0)
   m = m + 1;
else
   T.c = 0;
end

T.map = randi(t, m, q);
T.sg = sign(randn(m, q));
T.q = q;
T.c = c;
T.m = m;
T.t = t;

