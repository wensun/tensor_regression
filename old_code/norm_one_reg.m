function sketched_result = block_wise_tensor_sketch(A_cell, m, k)
    %A_cell: q matrices, m: target row number
    %k: divde each matrix A_i to k sub-matrix
    
    q = length(A_cell);
    ws = zeros(q,1);
    inds = ones(q,1); %capped by k.
    ns = zeros(q,1);
    rs = zeros(q,1);
    for i = 1 : q
        ns(i) = size(A_cell{i},1);
        rs(i) = size(A_cell{i},2);
        ws(i) = ns(i)/k;
    end
    n = prod(ns);
    r = prod(rs);
    sketched_result = zeros(k*m, r);
    
    pos = 1;  %pos in [1, k^q]
    while true
        sub_A_cell = {};
        for i = 1 : q
            A = A_cell{i};
            index = inds{i};
            row_start = (index-1)*ws(i);
            row_end = (index)*ws(i)
            sub_A_cell{i} = A(row_start:row_end, :);
        end
        [S, Cs] = apply_ppt_kron(sub_A_cell, m);
        sketched_result((pos-1)*m:pos*m, :) = S;
        
        pos = pos + 1;
        if prod(inds) == k^q
            break
        else
            inds = update_index(inds, k) % k is the cap for each index.
        end
    end
end
    
    
function new_inds = update_index(inds, k)
    new_inds = inds;
    q = size(inds);
    pos = -1;
    for i = q:-1:1
        if inds(i) < k
            pos = i;
            break
        end
    end
    new_inds(pos) = new_inds(pos)+1;
    if pos + 1 <= q
        new_inds(pos+1:q) = 1;
    end
end 
   

